package testvaddin;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.niknis.test.model.Student;
import com.niknis.test.service.StudentLocalServiceUtil;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Form;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class TestvaddinApplication extends Application {

	private final Form form = new Form();
	private StBean stBean = new StBean();

	public void init() {
		final Window mainWindow = new Window("Student Details");
		// Layout main = new VerticalLayout();
		setMainWindow(mainWindow);

		final BeanItemContainer<StBean> numbers = new BeanItemContainer<StBean>(StBean.class);
		numbers.addAll(getStudentList());
		final Table table = new Table("Students");
		table.setContainerDataSource(numbers);
		table.setVisibleColumns(new Object[] { "name", "role" });
		// When the user selects an item, show it in the form
		table.addListener(new Property.ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				// Close the form if the item is deselected
				if (event.getProperty().getValue() == null) {
					form.setVisible(false);
					return;
				}

				// Bind the form to the selected item
				form.setItemDataSource(numbers.getItem(table.getValue()));
				form.getField("id").setReadOnly(true);
				// form.setVisibleItemProperties(new Object[] {"name", "role"});
				form.setVisible(true);
			}
		});
		table.setSelectable(true);
		table.setImmediate(true);

		Button refreshTbl = new Button("refresh list");

		refreshTbl.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				final BeanItemContainer<StBean> numbers = new BeanItemContainer<StBean>(StBean.class);
				numbers.addAll(getStudentList());
				// table.setData(null);
				table.setContainerDataSource(numbers);
				table.setEnabled(true);
			}
		});

		CssLayout mainLayout = new CssLayout();
		mainLayout.setWidth("100%");
		mainLayout.addStyleName("flexwrap");
		form.setCaption("Enter Details");

		form.setDescription("Enter your Details");
		form.setImmediate(true);
		form.setWriteThrough(false);

		BeanItem item = new BeanItem(stBean);
		// Bind the bean item as the data source for the form.
		form.setItemDataSource(item);
		form.setVisibleItemProperties(new Object[] { "name", "role" });

		Button bt = new Button("save");

		bt.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.commit();
				String name = "";
				long role = 0l;
				long id = 0l;
				if (form.getItemProperty("id") != null && form.getItemProperty("id").getValue() != null) {
					id = GetterUtil.getLong(form.getField("id").getValue());
				}

				if (form.getField("name").getValue() != null) {
					name = form.getField("name").getValue().toString();
				}
				if (form.getField("role").getValue() != null) {
					role = GetterUtil.getLong(form.getField("role").getValue());
				}
				if (id > 0) {
					if (!"".equals(name) && role > 0) {
						try {
							Student st = StudentLocalServiceUtil.getStudent(id);
							st.setStName(name);
							st.setStRole(role);
							StudentLocalServiceUtil.updateStudent(st);
						} catch (SystemException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (PortalException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else if (!"".equals(name) && role > 0) {
					try {
						StudentLocalServiceUtil.addStudent(name, role);
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				mainWindow.showNotification("Record added, Please refresh list");
			}
		});
		// form.addComponent(bt);
		mainLayout.addComponent(refreshTbl);
		mainLayout.addComponent(table);
		mainLayout.addComponent(form);
		mainLayout.addComponent(bt);
		mainWindow.addComponent(mainLayout);
	}

	public List<StBean> getStudentList() {
		// TODO Auto-generated method stub
		List<StBean> stList = new ArrayList<StBean>();
		try {
			List<Student> students = StudentLocalServiceUtil.getStudents(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for (Student student : students) {
				if (student != null && student.getStudentId() > 0) {
					StBean st = new StBean();
					st.setId(student.getStudentId());
					st.setName(student.getStName());
					st.setRole(student.getStRole());
					stList.add(st);
				}
			}
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stList;
	}

}