/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.niknis.test.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.niknis.test.model.Student;
import com.niknis.test.service.base.StudentLocalServiceBaseImpl;

import testvaddin.StBean;

/**
 * The implementation of the student local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.niknis.test.service.StudentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Nikhil Nishchal
 * @see com.niknis.test.service.base.StudentLocalServiceBaseImpl
 * @see com.niknis.test.service.StudentLocalServiceUtil
 */
public class StudentLocalServiceImpl extends StudentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.niknis.test.service.StudentLocalServiceUtil} to access the student local service.
	 */
	
	public Student addStudent(String name, long role) throws SystemException {
		// TODO Auto-generated method stub
		long stId =
		        counterLocalService.increment(Student.class.getName());

		Student student = studentPersistence.create(stId);
		student.setStName(name);
		student.setStRole(role);
		return super.addStudent(student);
	}
}